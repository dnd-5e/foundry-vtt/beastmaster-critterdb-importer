import { ItemData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs'
import { DocumentConstructor } from '@league-of-foundry-developers/foundry-vtt-types/src/types/helperTypes'
import { normalizeName } from '@utils/string/normalize-name'

type SpellMap = { [x: string]: ItemData }

const getSpellsFromCollection = (collection?: WorldCollection<DocumentConstructor, string>): ItemData[] => {
  const spells: ItemData[] =
    collection?.filter((item: Item5e) => item.type === 'spell')?.map((item: Item5e) => item.data) ?? []
  return spells
}

const getSpellMap = async (): Promise<SpellMap> => {
  const packs: any[] = game.packs.filter((p) => p.documentName.toLowerCase() === 'item')

  const map: SpellMap = {}
  const spells = [
    ...(await getSpellsFromPacks(packs)).filter((s) => s !== null),
    ...getSpellsFromCollection(game.collections.get('Item')),
  ]

  for (const s of spells) {
    map[normalizeName(s.name)] = s
  }

  return map
}

const getSpellsFromPacks = async (
  packs: CompendiumCollection<CompendiumCollection.Metadata>[]
): Promise<ItemData[]> => {
  const promises: Promise<ItemData>[] = []
  for (const compendium of packs) {
    // retrieve the compendium index
    let index = await compendium.getIndex()

    let spellIndices = index.filter((idx) => idx.type === 'spell')

    // replace non-null values with the complete entity from the compendium
    for (const spell of spellIndices) {
      promises.push(
        new Promise((resolve) => {
          compendium.getDocument(spell._id).then((item) => {
            resolve(item?.data ?? null)
          })
        })
      )
    }
  }
  return Promise.all(promises)
}

export { getSpellMap, SpellMap }
