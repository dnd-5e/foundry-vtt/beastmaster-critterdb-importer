import { getEntityTemplate } from './get-entity-template'

const getFeatTemplate = (): data5e.Feat => getEntityTemplate('feat') as data5e.Feat

export { getFeatTemplate }
