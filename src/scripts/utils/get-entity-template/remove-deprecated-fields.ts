import { isDeprecated } from '@typing'
import { EntityTemplate } from '@typing'

const trimDeprecated = (data: EntityTemplate): EntityTemplate => {
  for (const prop in data) {
    if (isDeprecated(data[prop])) {
      delete data[prop]
    }
  }

  if (isDeprecated(data)) {
    delete data._deprecated
  }

  return data
}

export { trimDeprecated }
