import { getEntityTemplate } from './get-entity-template'

const getWeaponTemplate = (): data5e.Weapon => getEntityTemplate('weapon') as data5e.Weapon

export { getWeaponTemplate }
