import { getEntityTemplate } from './get-entity-template'

const getNpcTemplate = (): data5e.Npc => getEntityTemplate('npc') as data5e.Npc

export { getNpcTemplate }
