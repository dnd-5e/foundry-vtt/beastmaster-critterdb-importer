import deepMerge from 'deepmerge'
import { EntityTemplateMap, isEntityTemplateMap, isEntityTemplate, EntityTemplate } from '@typing'
import { trimDeprecated } from './remove-deprecated-fields'

const getEntityTemplate = (type: string): EntityTemplate => {
  if (isEntityTemplateMap(game.data.system.template)) {
    const templates: EntityTemplateMap = game.data.system.template

    for (const entityType of Object.values(templates)) {
      const typeTemplate = entityType[type]

      if (entityType.types.includes(type) && isEntityTemplate(typeTemplate)) {
        let obj: EntityTemplate = deepMerge({}, trimDeprecated(typeTemplate))

        if (obj.templates && entityType.templates) {
          for (const t of obj.templates) {
            obj = deepMerge(obj, trimDeprecated(entityType.templates[t]))
          }
          delete obj.templates
        }
        // store the result as JSON for easy cloning
        return obj
      }
    }
  }
  return {}
}

export { getEntityTemplate }
