import { Skill } from '@nystik/critterdb-parser'

const skillLookup = (skills: Skill[]) => (name: string) => skills.find((s) => s.name === name)

export { skillLookup }
