import { SavingThrow } from '@nystik/critterdb-parser'

const savingThrowLookup = (saves: SavingThrow[]) => (ability: string) => saves.find((s) => s.ability === ability)

export { savingThrowLookup }
