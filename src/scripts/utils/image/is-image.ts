const isFileImage = (file: File): boolean => {
  return file && file['type'].split('/')[0] === 'image'
}

export { isFileImage }
