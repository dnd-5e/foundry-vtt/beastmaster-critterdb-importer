import { fileFromBlob } from '@nystik/foundry-file-utils'
import smartcrop, { CropResult } from 'smartcrop'
import { isFileImage } from './is-image'

// use image/png for compatibility
const cropImage = async (image: HTMLImageElement, type: string, { topCrop }: CropResult): Promise<Blob | null> => {
  const canvas = document.createElement('canvas')
  canvas.width = topCrop.width
  canvas.height = topCrop.height
  const ctx = canvas.getContext('2d')
  ctx!.drawImage(image, topCrop.x, topCrop.y, topCrop.width, topCrop.height, 0, 0, topCrop.width, topCrop.height)
  return await new Promise(function (resolve) {
    canvas.toBlob(resolve, type, 1)
  })
}

const AutoCropImage = async (file: File, size: number): Promise<File> => {
  if (!isFileImage) {
    throw new Error(`The file is not an image.`)
  }

  return await new Promise<File>((resolve, reject) => {
    var reader = new FileReader()

    reader.onload = async function (e: any) {
      const img = new Image()
      img.src = e.target.result
      await img.decode()
      const crop = await smartcrop.crop(img, { height: size, width: size, minScale: 0.8, ruleOfThirds: true })
      const blob = await cropImage(img, 'image/png', crop)
      if (!blob) {
        return reject(new Error('Unable to crop image'))
      }
      return resolve(fileFromBlob(blob, file.name))
    }
    reader.readAsDataURL(file)
  })
}

export { AutoCropImage }
