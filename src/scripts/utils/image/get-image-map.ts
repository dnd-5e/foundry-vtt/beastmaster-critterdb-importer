import { ItemData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs'
import { normalizeName } from '@utils'

type ImageMap = { [x: string]: string }

const getImageMap = async (): Promise<ImageMap> => {
  const packs: any[] = game.packs.filter((p) => p.documentName.toLowerCase() === 'item')

  const map: ImageMap = {}
  const items = [...(await getImagesFromPacks(packs)).filter((i) => i !== null)]

  for (const i of items) {
    map[normalizeName(i.name)] = i.img ?? ''
  }

  return map
}

const getImage = (imageMap: ImageMap, query: string): string =>
  imageMap[normalizeName(query)]
  || imageMap[`${normalizeName(query)}s`]
  || searchImage(imageMap, query)
  || reverseSearchImage(imageMap, query)
  || splitSearchImage(imageMap, query)

const searchImage = (imageMap: ImageMap, query: string): string =>
  Object.entries(imageMap).find(([k, v]) => k.includes(normalizeName(query)))?.[1] ?? ''

const reverseSearchImage = (imageMap: ImageMap, query: string): string =>
  Object.entries(imageMap).find(([k, v]) => normalizeName(query).includes(k))?.[1] ?? ''

const splitSearchImage = (imageMap: ImageMap, query: string): string =>
  Object.entries(imageMap).find(([k, v]) => query.split(' ').some((p) => k.includes(p.toLowerCase())))?.[1] ?? ''

const getImagesFromPacks = async (
  packs: CompendiumCollection<CompendiumCollection.Metadata>[]
): Promise<ItemData[]> => {
  const promises: Promise<ItemData>[] = []
  for (const compendium of packs) {
    // retrieve the compendium index
    let indices = await compendium.getIndex()

    for (const item of indices) {
      promises.push(
        new Promise((resolve) => {
          compendium.getDocument(item._id).then((item) => {
            resolve((item as Item5e)?.data ?? null)
          })
        })
      )
    }
  }
  return Promise.all(promises)
}

export { getImageMap, searchImage, getImage, ImageMap }
