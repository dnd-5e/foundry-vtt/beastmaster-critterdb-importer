const getConfig = (): ExtendedDND5e => {
  return game.dnd5e.config as ExtendedDND5e
}

export { getConfig }
