const normalizeName = (name: string): string => name.toLowerCase().replace(/\W/g, '')

export { normalizeName }
