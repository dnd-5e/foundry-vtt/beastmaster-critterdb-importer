import { SavingThrow } from '@nystik/critterdb-parser'

const calculateSaveBonus =
  (abilityMod: number, proficiency: number) =>
  (save?: SavingThrow): number => {
    if (!save) {
      return 0
    }

    const { proficient, value: expected } = save
    const prof = proficient ? proficiency : 0

    const derived = abilityMod + prof

    const diff = expected - derived

    return diff
  }

export { calculateSaveBonus }
