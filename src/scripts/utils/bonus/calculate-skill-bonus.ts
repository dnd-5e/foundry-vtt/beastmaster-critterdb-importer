import { Skill } from '@nystik/critterdb-parser'

const calculateSkillBonus =
  (abilityMod: number, proficiency: number) =>
  (skill?: Skill): number => {
    if (!skill) {
      return 0
    }

    const { proficient, expertise, value: expected } = skill
    const prof = expertise ? proficiency * 2 : proficient ? proficiency : 0

    const derived = abilityMod + prof

    const diff = expected - derived

    return diff
  }

export { calculateSkillBonus }
