const calculateAttackBonus =
  (abilityMod: number, proficiency: number) =>
  (expected?: number): number => {
    if (!expected) {
      return 0
    }

    const derived = abilityMod + proficiency

    const diff = expected - derived

    return diff
  }

export { calculateAttackBonus }
