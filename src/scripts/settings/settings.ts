import { MODULE_NAME } from '../module-config'

const registerSettings = () => {
  game.settings.register(MODULE_NAME, 'testSettings', {
    name: 'Test Setting',
    hint: 'Description',
    scope: 'world',
    config: true,
    type: String,
    choices: {
      // If choices are defined, the resulting setting will be a select menu
      a: 'Option A',
      b: 'Option B',
    },
    default: 'a', // The default value for the setting
    onChange: (value) => {
      // A callback function which triggers when the setting is changed
      console.log(value)
    },
  })
}

export { registerSettings }
