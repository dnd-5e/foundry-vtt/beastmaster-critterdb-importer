const SIZE_MAP: { [x: string]: DND5e.Size } = {
  gargantuan: 'grg',
  huge: 'huge',
  large: 'lg',
  medium: 'med',
  small: 'sm',
  tiny: 'tiny',
}

export { SIZE_MAP }
