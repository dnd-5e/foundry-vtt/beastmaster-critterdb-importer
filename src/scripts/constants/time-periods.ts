const TIME_PERIOD_MAP: { [x: string]: DND5e.TimePeriod } = {
  turn: 'turn',
  turns: 'turn',
  round: 'round',
  rounds: 'round',
  minute: 'minute',
  minutes: 'minute',
  hour: 'hour',
  hours: 'hour',
  day: 'day',
  days: 'day',
  month: 'month',
  months: 'month',
  year: 'year',
  years: 'year',
}

export { TIME_PERIOD_MAP }
