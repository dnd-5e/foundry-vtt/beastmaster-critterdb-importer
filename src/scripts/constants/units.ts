const UNIT_MAP: { [x: string]: ExtendedDND5e.Unit } = {
  ft: 'ft',
  feet: 'ft',
  foot: 'ft',
  mi: 'mi',
  mile: 'mi',
  miles: 'mi',
  m: 'm',
  meter: 'm',
  meters: 'm',
  km: 'km',
  kilometer: 'km',
  kilometers: 'km',
}

export { UNIT_MAP }
