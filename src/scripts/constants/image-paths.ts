const IMAGE_PATHS: Record<string, string> = {
  spellcasting: 'systems/dnd5e/icons/skills/violet_08.jpg',
}

export { IMAGE_PATHS }
