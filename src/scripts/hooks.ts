import archmage from '../data/archmage.json'
import nighthag from '../data/night-hag.json'
import beholder from '../data/beholder.json'
import warlock from '../data/warlock.json'
import kraken from '../data/kraken.json'
import { Parser, getDefaultOptions, ParserOptions, Spell } from '@nystik/critterdb-parser'
import { getImage, getImageMap, getSpellMap } from '@utils'
import { createDirectory, downloadFile, uploadFile, DataSource } from '@nystik/foundry-file-utils'
import { importCritters } from './import/import-critters'
//import { injectFilePickerComponent } from './settings/file-picker/file-picker'
//import { FileMeta, FilePicker as FilePickerSetting } from './settings/file-picker/file-picker-types'
//import { FileUtils } from '@nystik/foundry-file-utils'

const options: ParserOptions = {
  ...getDefaultOptions(),
  conformProficiency: 'conformProfToCr',
  deriveProficiencyFromModifiers: true,
}

const defaultPath = { path: 'critter-db/images', activeSource: 'data', bucket: null }

const registerHooks = () => {
  console.log('Hello World! This code runs immediately when the file is loaded.')

  Hooks.on('init', function () {
    console.log('This code runs once the Foundry VTT software begins its initialization workflow.')
  })

  Hooks.on('fileUtilsReady', (fileUtils) => {
    game.settings.register('test-module', 'file-picker', {
      name: 'Pick a directory',
      hint: 'A description of the registered setting and its behavior.',
      scope: 'client',
      config: true,
      type: fileUtils.Types.FilePicker('folder'),
      default: defaultPath,
    })
  })

  /*Hooks.on('renderSettingsConfig', (_app: any, html: string, _user: any) => {
    const inputElement = $(`<input type="file" id="input">`)
    inputElement.on('change', handleFiles)
    async function handleFiles(this: any) {
      const fileList = this.files 
      console.log(fileList)
      console.log(await uploadFile('s3', 'test/path', fileList[0], { bucket: 'fvtt-test' }))
    }
    $(html).append(inputElement)
  })*/

  Hooks.on('ready', async function () {
    /*console.log(file)
    var reader = new FileReader()
    // Note: addEventListener doesn't work in Google Chrome for this event
    reader.onload = function (evt: any) {
      const img = new Image()
      img.onload = function () {
        console.log(img)
        const f2 = fileFromBlob(dataURItoBlob(img.src), file!.name)
        console.log(f2)
      }
      img.src = evt.target.result
    }
    reader.readAsDataURL(file!)*/
    //console.log(await createDirectory('data', 'test/path/to/create'))

    game.settings.register('test-module', 'file-picker2', {
      name: 'Image Picker',
      hint: 'A description of the registered setting and its behavior.',
      scope: 'client',
      config: true,
      type: window.FileUtils.Types.FilePicker('image'),
      default: defaultPath,
    })

    const meta: CompendiumCollection.Metadata = {
      entity: 'Actor',
      name: 'test-compendium',
      label: 'Test Compendium Label',
      private: false,
      package: 'world',
      path: '',
    }

    //console.log(await createDirectory('s3', 'test-dir', { bucket: 'fvtt-test' }))

    /*console.log('Feat Template', JSON.stringify(getFeatTemplate()))*/
    const spellMap = await getSpellMap()
    const imageMap = await getImageMap()
    console.log(imageMap)

    const portraitDir = { activeSource: 'data' as DataSource, path: 'critterdb-images', bucket: '' }
    const tokenDir = { activeSource: 'data' as DataSource, path: 'critterdb-tokens', bucket: '' }

    await createDirectory(portraitDir.activeSource, portraitDir.path, { bucket: '' })
    await createDirectory(tokenDir.activeSource, tokenDir.path, { bucket: '' })

    const importer = importCritters(spellMap, imageMap)(portraitDir, tokenDir)

    try {
      const { imgPromises, actorPromises } = await importer([beholder])

      imgPromises.map((p) => {
        p.then((r) => console.log(r))
      })

      actorPromises.map((p) => {
        p.then((a) => console.log(a))
      })
    } catch (e) {
      console.log(e)
    }
    /*await CompendiumCollection.createCompendium(meta);

    console.log("GAME", game);

    let comp: CompendiumCollection<CompendiumCollection.Metadata> =
      await game.packs.get("world.test-compendium")!;

    //@ts-ignore
    const actor = await Actor.create(
      {
        name: "Test",
        type: "npc",
      },
      { temporary: true, displaySheet: false }
    );

    comp.importDocument(actor!);*/
  })
}

export { registerHooks }
