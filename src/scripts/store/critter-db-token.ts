const ACCESS_TOKEN_STORE_KEY = 'critterdb_access_token'

const storeCritterAccessToken = (token: string): void => {
  localStorage.setItem(ACCESS_TOKEN_STORE_KEY, token)
}

const getCritterAccessTokenFromStore = (): string => {
  return localStorage.getItem(ACCESS_TOKEN_STORE_KEY) ?? ''
}

export { storeCritterAccessToken, getCritterAccessTokenFromStore }
