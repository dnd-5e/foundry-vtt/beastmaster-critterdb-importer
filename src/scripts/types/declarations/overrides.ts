declare global {
  interface LenientGlobalVariableTypes {
    game: never // the type doesn't matter
  }
  interface Window {
    FileUtils: any
  }
  interface ExtendedDND5e extends DND5e {
    distanceUnits: Record<DND5e.DistanceUnit, string> & ExtendedDND5e['movementUnits']
    movementUnits: Record<ExtendedDND5e.Unit, string>
  }

  namespace ExtendedDND5e {
    type Unit = 'ft' | 'mi' | 'm' | 'km'
  }

  type DamagePart = [string, DND5e.DamageResistanceType]

  namespace data5e {
    interface Feat {
      uses: {
        per: string | null
        value: number
        max: number
      }
      target: {
        value: number | null
        width: number | null
        units: DND5e.DistanceUnit | ExtendedDND5e.Unit
        type: DND5e.TargetType
      }
      damage: {
        parts: DamagePart[]
        versatile: string
      }
      range: {
        value?: number | null
        long?: number | null
        units: DND5e.DistanceUnit | ExtendedDND5e.Unit
      }
      duration: {
        value?: number | null
        units: DND5e.TimePeriod
      }
      save: {
        ability?: string | null
        dc?: number | null
        scaling: string
      }
      activation?: {
        type?: DND5e.AbilityActivationType | null
        cost?: number | null
      }
      actionType: DND5e.ActionType
      ability: DND5e.AbilityType
      consume: {
        type: DND5e.ConsumeType
        target: string
        amount: number
      }
    }

    interface Weapon {
      uses: {
        per: string | null
        value: number
        max: number
      }
      target: {
        value: number | null
        width: number | null
        units: DND5e.DistanceUnit | ExtendedDND5e.Unit
        type: DND5e.TargetType
      }
      damage: {
        parts: DamagePart[]
        versatile: string
      }
      range: {
        value?: number | null
        long?: number | null
        units: DND5e.DistanceUnit | ExtendedDND5e.Unit
      }
      duration: {
        value?: number | null
        units: DND5e.TimePeriod
      }
      save: {
        ability?: string | null
        dc?: number | null
        scaling: string
      }
      activation?: {
        type?: DND5e.AbilityActivationType | null
        cost?: number | null
      }
      actionType: DND5e.ActionType
      ability: DND5e.AbilityType
      consume: {
        type: DND5e.ConsumeType
        target: string
        amount: number
      }
    }

    interface Spell {
      ability: string | null
      save: {
        dc: number | null
        scaling: string
        ability: string
      }
      uses: {
        max: number
        value: number
        per: string
      }
    }

    interface ChaClass {
      flat?: number | null
      calc?: string
      formula?: string
    }

    interface PurpleHP {
      formula?: string
    }

    interface Skill {
      bonuses?: { check?: number; passive?: number }
    }

    interface Cha {
      bonuses?: {
        save?: number
        check?: number
      }
    }
  }
}

export {}
