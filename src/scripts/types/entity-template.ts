import { isObject } from './object'

interface EntityTemplate extends Record<string, any> {
  templates?: string[]
}

interface EntityTemplateCategory {
  types: string[]
  templates?: Record<string, Record<string, any>>
  [key: string]: EntityTemplate | unknown[] | undefined
}

interface EntityTemplateMap {
  [key: string]: EntityTemplateCategory
}

const isEntityTemplate = (object: unknown): object is EntityTemplate => {
  return isObject(object) && object.templates !== undefined && Array.isArray(object.templates)
}

const isEntityTemplateCategory = (template: unknown): template is EntityTemplateCategory => {
  return isObject(template) && template.types !== undefined && Array.isArray(template.types)
}

const isEntityTemplateMap = (map: unknown): map is EntityTemplateMap => {
  return isObject(map) && Object.values(map).every((t) => isEntityTemplateCategory(t))
}

export type { EntityTemplateCategory, EntityTemplateMap, EntityTemplate }
export { isEntityTemplateCategory, isEntityTemplateMap, isEntityTemplate }
