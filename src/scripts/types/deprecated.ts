import { isObject } from './object'

interface Deprecated extends Record<string, unknown> {
  _deprecated?: true
}

const isDeprecated = (object: unknown): object is Deprecated => {
  return (
    isObject(object)
    && Object.prototype.hasOwnProperty.call(object, '_deprecated')
    && (object as Deprecated)._deprecated === true
  )
}

export { isDeprecated }
