const isObject = (obj: unknown): obj is Record<string, unknown> => {
  return obj instanceof Object
}

export { isObject }
