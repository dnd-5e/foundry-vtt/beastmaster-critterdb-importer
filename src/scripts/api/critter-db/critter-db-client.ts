import { getCritterAccessTokenFromStore } from 'src/scripts/store'
import { isJwtExpired } from '../api-utils/is-jwt-expired'

const getAuthenticatedClient = async (username?: string, password?: string) => {
  let token = getCritterAccessTokenFromStore()

  if (!token || isJwtExpired(token)) {
    //TODO: get credentials from settings or credentials api
    //token = await authenticate(username, password)
  }
}

export {}
