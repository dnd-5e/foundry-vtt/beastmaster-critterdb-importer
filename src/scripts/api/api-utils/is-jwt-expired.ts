import { decode } from 'jsonwebtoken'

interface JWTPayload {
  sub: string
  iat: number
  exp: number
  aud: string
  iss: string
}

const isExpired = (exp: number): boolean => {
  return Date.now() >= exp * 1000
}

const isJwtExpired = (token: string): boolean => {
  const { exp } = decode(token) as JWTPayload
  return isExpired(exp)
}

export { isJwtExpired }
