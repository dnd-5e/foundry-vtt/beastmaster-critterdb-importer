import { createAction } from '@factories/create-action/create-action'
import { createSpellcasting, createSpells } from '@factories/create-spells'
import { ActorDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/actorData'
import { Statblock5e } from '@nystik/critterdb-parser'
import { getImage, ImageMap, SpellMap } from '@utils'

const composeActor =
  (spells: SpellMap, icons: ImageMap) =>
  (collection: any) =>
  (statblock: Statblock5e) =>
  async (actorData: ActorDataConstructorData): Promise<StoredDocument<Actor5e> | undefined> => {
    const {
      stats: {
        proficiencyBonus,
        abilityScoreModifiers: { strength, dexterity },
      },
      abilities: { spellcastingFeatures, actions, legendaryActions, features },
    } = statblock

    const abilityMod = Math.max(strength, dexterity)
    const ability = strength > dexterity ? 'str' : 'dex'

    //TODO: create in collection
    const actor = await Actor.create(actorData)

    if (!actor) {
      throw new Error('unable to create actor')
    }

    const spellDocuments = createSpells(statblock)(spells, spellcastingFeatures)

    const actionDocuments = [...actions, ...legendaryActions.actions].map((a) =>
      createAction(ability, abilityMod, proficiencyBonus)(a, getImage(icons, a.name))
    )

    const featureDocuments = [...features].map((a) =>
      createAction(ability, abilityMod, proficiencyBonus)(a, getImage(icons, a.name))
    )

    const spellcastingDocuments = [...spellcastingFeatures].map((a) => createSpellcasting(a, getImage(icons, a.name)))

    actor.createEmbeddedDocuments('Item', [
      ...spellDocuments,
      ...actionDocuments,
      ...featureDocuments,
      ...spellcastingDocuments,
    ])

    return actor
  }

export { composeActor }
