import { DataSource, downloadFile, uploadFile } from '@nystik/foundry-file-utils'
import { Types } from '@nystik/foundry-file-utils'
import { AutoCropImage } from '@utils/image'

type FileMeta = Types.FileMeta

type ImageResponseType =
  | false
  | void
  | (Response & {
      path: string
      message?: string | undefined
    })

interface UploadResponse {
  imgPath: string
  tokenPath: string
  imgUpload: Promise<ImageResponseType>
  tokenUpload: Promise<ImageResponseType>
}

const upload = ({ activeSource, path, bucket }: FileMeta, file: File): Promise<ImageResponseType> => {
  bucket = bucket ?? ''
  return uploadFile(activeSource as DataSource, path, file, { bucket: bucket })
}

const uploadImage =
  (imgLoc: FileMeta, tokenLoc: FileMeta) =>
  (image: File): UploadResponse => {
    const imgPath = `${imgLoc.path}/${image.name}`
    const tokenPath = `${tokenLoc.path}/${image.name}`

    const imgUpload = upload(imgLoc, image)

    const tokenUpload = new Promise<ImageResponseType>(async (resolve, reject) => {
      try {
        const cropped = await AutoCropImage(image, 200)
        const result = await upload(tokenLoc, cropped)
        resolve(result)
      } catch (e) {
        reject(e)
      }
    })

    return {
      imgPath,
      tokenPath,
      imgUpload,
      tokenUpload,
    }
  }

const imageHandler = (imgLoc: FileMeta, tokenLoc: FileMeta) => {
  const uploader = uploadImage(imgLoc, tokenLoc)
  return async (imageUrl: string): Promise<UploadResponse | null> => {
    // TODO: check if image file already exists
    const image = imageUrl ? await downloadFile(imageUrl) : null

    if (image) {
      const response = uploader(image)
      return response
    }
    return null
  }
}

export { imageHandler, UploadResponse, ImageResponseType }
