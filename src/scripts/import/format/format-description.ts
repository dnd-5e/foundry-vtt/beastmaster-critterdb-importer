const paragraph = (text) => `<p>${text}</p>`

const italicizeAttack = (text: string) => {
  return text.replace(/(melee|ranged)(\s\w+)+:/i, `<i>$&</i>`).replace('Hit:', `<i>Hit:</i>`)
}

const splitParagraphs = (text: string) => {
  const parts = text.split('\n')
  return parts.map((s) => paragraph(s)).join('\n')
}

const formatDescription = (text: string) => {
  return splitParagraphs(italicizeAttack(text))
}

export { formatDescription }
