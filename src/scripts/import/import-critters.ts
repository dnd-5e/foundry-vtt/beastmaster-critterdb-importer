import { createAction } from '@factories/create-action/create-action'
import { createActor } from '@factories/create-actor/create-actor'
import { createToken } from '@factories/create-actor/create-token'
import { createSpellcasting, createSpells } from '@factories/create-spells'
import { ActorDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/actorData'
import { critterDB, getDefaultOptions, Parser, ParserOptions, Statblock5e } from '@nystik/critterdb-parser'
import { DataSource, downloadFile, uploadFile } from '@nystik/foundry-file-utils'
import { Types } from '@nystik/foundry-file-utils'
import { getImage, ImageMap, SpellMap } from '@utils'
import { AutoCropImage } from '@utils/image/auto-crop-image'
import { composeActor } from './import-utils'
import { ImageResponseType, UploadResponse } from './import-utils/handle-image'

type FileMeta = Types.FileMeta

const { parseCritter, normalizeCritter } = Parser

interface ImportProgress {
  imageUpload: Promise<ImageResponseType>
  tokenUpload: Promise<ImageResponseType>
  actorCreation: Promise<StoredDocument<Actor5e> | undefined>
}

const parser = (options: ParserOptions) => (data: critterDB.Critter) => {
  return parseCritter(options)(normalizeCritter(data))
}

const importer =
  (spells: SpellMap, icons: ImageMap) =>
  (handleImage: (url: string) => Promise<UploadResponse>) =>
  async (critter: critterDB.Critter): Promise<ImportProgress> => {
    //TODO: load from settings
    const parseOptions: ParserOptions = {
      ...getDefaultOptions(),
      conformProficiency: 'conformProfToCr',
      deriveProficiencyFromModifiers: true,
    }

    const parse = parser(parseOptions)
    const actorFactory = composeActor(spells, icons)(null)

    const parsed = parse(critter)
    const {
      _id,
      statblock,
      statblock: { imageUrl },
      bestiaryId,
      publishedBestiaryId,
    } = parsed

    const { imgPath, tokenPath, imgUpload, tokenUpload } = await handleImage(imageUrl)

    const actorData = createActor(parsed, imgPath)
    actorData.token = createToken(parsed, tokenPath)

    const actorCreation = actorFactory(statblock)(actorData)

    return {
      actorCreation,
      imageUpload: imgUpload,
      tokenUpload: tokenUpload,
    }
  }

const importCritters =
  (spells: SpellMap, icons: ImageMap) =>
  (imgLoc: FileMeta, tokenLoc: FileMeta) =>
  async (critters: critterDB.Critter[]) => {
    const imgPromises: Promise<ResponseType>[] = []
    const actorPromises: Promise<StoredDocument<Actor5e> | undefined>[] = []

    for (const critter of critters) {
    }

    return {
      imgPromises,
      actorPromises,
    }
  }

export { importCritters }
