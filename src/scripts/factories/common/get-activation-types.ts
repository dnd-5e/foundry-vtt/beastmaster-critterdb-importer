import { getConfig } from '@utils'

const getActivationTypes = (): DND5e['abilityActivationTypes'] => {
  const config = getConfig()

  return config.abilityActivationTypes
}

const getActivationTypesKeys = (): DND5e.AbilityActivationType[] => {
  return Object.keys(getActivationTypes()) as DND5e.AbilityActivationType[]
}

const mapActivationType = (activation: string): DND5e.AbilityActivationType | null => {
  activation = activation === 'other' ? 'special' : activation
  return getActivationTypesKeys().find((c) => c === activation) ?? null
}

export { getActivationTypes, getActivationTypesKeys, mapActivationType }
