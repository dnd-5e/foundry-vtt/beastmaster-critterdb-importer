import { UNIT_MAP } from '@constants'
import { getConfig } from '@utils'

const getUnits = (): ExtendedDND5e['distanceUnits'] => {
  const config = getConfig()

  return config.distanceUnits
}

const getUnitsKeys = (): (DND5e.DistanceUnit | ExtendedDND5e.Unit)[] => {
  return Object.keys(getUnits()) as (DND5e.DistanceUnit | ExtendedDND5e.Unit)[]
}

const mapUnit = (unit: string): DND5e.DistanceUnit | ExtendedDND5e.Unit => {
  return getUnitsKeys().find((c) => c === UNIT_MAP[unit]) ?? 'ft'
}

export { getUnits, getUnitsKeys, mapUnit }
