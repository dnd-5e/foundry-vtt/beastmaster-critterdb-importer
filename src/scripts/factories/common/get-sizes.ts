import { SIZE_MAP } from '@constants'
import { getConfig } from '@utils'

const getSizes = (): DND5e['actorSizes'] => {
  const config = getConfig()

  return config.actorSizes
}

const getSizesKeys = (): DND5e.Size[] => {
  return Object.keys(getSizes()) as DND5e.Size[]
}

const mapSize = (size: string): DND5e.Size => {
  return getSizesKeys().find((c) => c === SIZE_MAP[size]) ?? 'med'
}

export { getSizes, getSizesKeys, mapSize }
