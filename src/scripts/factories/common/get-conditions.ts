import { getConfig } from '@utils'

const getConditions = (): DND5e['conditionTypes'] => {
  const config = getConfig()

  return config.conditionTypes
}

const getConditionsKeys = (): DND5e.ConditionType[] => {
  return Object.keys(getConditions()) as DND5e.ConditionType[]
}

export { getConditions, getConditionsKeys }
