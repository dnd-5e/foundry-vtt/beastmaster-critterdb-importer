import { getConfig } from '@utils'

const getLanguages = (): DND5e['languages'] => {
  const config = getConfig()

  return config.languages
}

const getLanguagesKeys = (): DND5e.Language[] => {
  return Object.keys(getLanguages()) as DND5e.Language[]
}

export { getLanguages, getLanguagesKeys }
