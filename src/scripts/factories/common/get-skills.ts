import { getConfig, normalizeName } from '@utils'

const getSkillTypes = (): DND5e['skills'] => {
  const config = getConfig()

  return config.skills
}

const getSkillTypesKeys = (): DND5e.SkillType[] => {
  return Object.keys(getSkillTypes()) as DND5e.SkillType[]
}

const mapSkillType = (skill: string): DND5e.SkillType | undefined => {
  return (
    (Object.entries(getSkillTypes()).find(
      ([, v]) => normalizeName(v) === normalizeName(skill)
    )?.[0] as DND5e.SkillType) || undefined
  )
}

export { getSkillTypes, getSkillTypesKeys, mapSkillType }
