import { getConfig } from '@utils'

const getDamageTypes = (): DND5e['damageResistanceTypes'] => {
  const config = getConfig()

  return config.damageResistanceTypes
}

const getDamageTypesKeys = (): DND5e.DamageResistanceType[] => {
  return Object.keys(getDamageTypes()) as DND5e.DamageResistanceType[]
}

const mapDamageType = (type: string): DND5e.DamageResistanceType => {
  return getDamageTypesKeys().find((c) => c === (type as DND5e.DamageResistanceType)) ?? 'none'
}

export { getDamageTypes, getDamageTypesKeys, mapDamageType }
