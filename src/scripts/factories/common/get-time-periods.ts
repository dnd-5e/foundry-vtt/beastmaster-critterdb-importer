import { TIME_PERIOD_MAP } from '@constants'
import { getConfig } from '@utils'

const getTimePeriods = (): DND5e['timePeriods'] => {
  const config = getConfig()

  return config.timePeriods
}

const getTimePeriodsKeys = (): DND5e.TimePeriod[] => {
  return Object.keys(getTimePeriods()) as DND5e.TimePeriod[]
}

const mapTimePeriod = (unit: string): DND5e.TimePeriod => {
  return getTimePeriodsKeys().find((c) => c === TIME_PERIOD_MAP[unit]) ?? 'minute'
}

export { getTimePeriods, getTimePeriodsKeys, mapTimePeriod }
