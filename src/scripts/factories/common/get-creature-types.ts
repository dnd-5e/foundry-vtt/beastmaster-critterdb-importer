import { getConfig } from '@utils'

const getCreatureTypes = (): DND5e['creatureTypes'] => {
  const config = getConfig()

  return config.creatureTypes
}

const getCreatureTypesKeys = (): DND5e.CreatureTypes[] => {
  return Object.keys(getCreatureTypes()) as DND5e.CreatureTypes[]
}

const mapCreatureType = (creature: string): DND5e.CreatureTypes | '' => {
  return getCreatureTypesKeys().find((c) => c === (creature as DND5e.CreatureTypes)) ?? ''
}

export { getCreatureTypes, getCreatureTypesKeys, mapCreatureType }
