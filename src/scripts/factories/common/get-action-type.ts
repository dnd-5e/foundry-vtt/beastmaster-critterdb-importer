import { getConfig } from '@utils'

const getActionTypes = (): DND5e['itemActionTypes'] => {
  const config = getConfig()

  return config.itemActionTypes
}

const getActionTypesKeys = (): DND5e.ActionType[] => {
  return Object.keys(getActionTypes()) as DND5e.ActionType[]
}

const mapActionType = (action: string): DND5e.ActionType | undefined => {
  return getActionTypesKeys().find((c) => c === action) ?? undefined
}

export { getActionTypes, getActionTypesKeys, mapActionType }
