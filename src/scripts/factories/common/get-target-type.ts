import { TARGET_TYPE_MAP } from '@constants'
import { getConfig } from '@utils'

const getTargetTypes = (): DND5e['targetTypes'] => {
  const config = getConfig()

  return config.targetTypes
}

const getTargetTypesKeys = (): DND5e.TargetType[] => {
  return Object.keys(getTargetTypes()) as DND5e.TargetType[]
}

const mapTargetType = (target: string): DND5e.TargetType | undefined => {
  return getTargetTypesKeys().find((c) => c === TARGET_TYPE_MAP[target]) ?? undefined
}

export { getTargetTypes, getTargetTypesKeys, mapTargetType }
