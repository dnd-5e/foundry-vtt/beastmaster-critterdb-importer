import { mapDamageType } from '@factories/common'
import { AbilitySave, DamageRoll, Roll } from '@nystik/critterdb-parser'

const getDamageString = (formula: string, damageType?: string) => `${formula}${damageType ? `[${damageType}]` : ''}`

const sortRolls = (
  attackDamage: DamageRoll[],
  otherDamage: DamageRoll[],
  otherRolls: Roll[],
  savingThrows: AbilitySave[]
): { damageParts: DamagePart[]; otherFormula: string; formulaSave?: AbilitySave } => {
  const ongoing = [...attackDamage, ...otherDamage].filter((d) => d.ongoing)
  const atk = attackDamage.filter((d) => !d.ongoing)
  const other = otherDamage.filter((d) => !d.ongoing)

  const formulaSave = savingThrows.find((s) => s.damage.length === 1)

  let otherRoll =
    otherRolls.length > 0
      ? (otherRolls[0] as DamageRoll)
      : ongoing.length === 1 && [...attackDamage, ...otherDamage].length > 0
      ? ongoing.pop()
      : formulaSave?.damage && [...attackDamage, ...otherDamage].length > 0
      ? formulaSave?.damage.pop()
      : other.length === 1 && attackDamage.length > 0
      ? other.pop()
      : undefined

  const damageParts = [...atk, ...other, ...ongoing, ...savingThrows.flatMap((s) => s.damage)].map(
    (d) => [d.formula, mapDamageType(d.damageType)] as DamagePart
  )
  const otherFormula = otherRoll ? getDamageString(otherRoll.formula, otherRoll?.damageType ?? '') : ''

  return {
    damageParts,
    otherFormula,
    formulaSave,
  }
}

export { sortRolls }
