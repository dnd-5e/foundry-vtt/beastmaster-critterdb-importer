import { Action } from '@nystik/critterdb-parser'
import { getWeaponTemplate } from '@utils'
import { createFeat } from './create-feat'

const createWeapon =
  (ability: string, abilityMod: number, proficiency: number) =>
  (action: Action): DeepPartial<data5e.Weapon> => {
    const base = getWeaponTemplate()

    const feat = createFeat(ability, abilityMod, proficiency)(action)

    const { name, reach, attack } = action

    const { melee, ranged, spell, weapon, versatile, bonus, damage: attackDamage } = attack ?? {}

    const weaponType: DND5e.WeaponType = melee ? 'simpleM' : ranged ? 'simpleR' : 'natural'

    const actionType = !attack
      ? feat.actionType
      : weapon && melee
      ? 'mwak'
      : weapon && ranged
      ? 'rwak'
      : spell && melee
      ? 'msak'
      : spell && ranged
      ? 'rsak'
      : feat.actionType

    const hasReach = reach.value > 5

    const data: DeepPartial<data5e.Weapon> = {
      ...feat,
      actionType,
      weaponType,
      proficient: true,
      equipped: true,
      identified: true,
      properties: {
        rch: hasReach,
        ver: feat?.damage?.versatile ? true : false,
      },
    }

    return { ...base, ...data }
  }

export { createWeapon }
