import { ItemDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/itemData'
import { Action } from '@nystik/critterdb-parser'
import { createFeat } from './create-feat'
import { createWeapon } from './create-weapon'

const createAction =
  (ability: string, abilityMod: number, proficiency: number) =>
  (action: Action, img?: string): DeepPartial<ItemDataConstructorData> => {
    const { name, attack } = action

    const type = attack ? 'weapon' : 'feat'
    const factory = attack ? createWeapon : createFeat

    const data = factory(ability, abilityMod, proficiency)(action)

    return {
      name,
      type,
      img,
      data,
    }
  }

export { createAction }
