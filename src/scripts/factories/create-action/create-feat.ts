import { mapActivationType, mapTargetType, mapTimePeriod, mapUnit } from '@factories/common'
import { Action } from '@nystik/critterdb-parser'
import { calculateAttackBonus, getFeatTemplate } from '@utils'
import { formatDescription } from 'src/scripts/import/format/format-description'
import { sortRolls } from './sort-rolls'

const createFeat =
  (ability: string, abilityMod: number, proficiency: number) =>
  (action: Action): DeepPartial<data5e.Feat> => {
    const base = getFeatTemplate()

    const {
      name,
      description,
      activation: actionActivation,
      range: actionRange,
      reach,
      cost,
      attack,
      damageRolls,
      duration: actionDuration,
      limitedUses,
      linked,
      otherRolls,
      recharge,
      savingThrows,
      target: { area, count, type: targetType },
    } = action

    const { melee, ranged, spell, weapon, versatile, bonus, damage: attackDamage } = attack ?? {}

    const attackBonus = calculateAttackBonus(abilityMod, proficiency)(bonus) || undefined

    const target = {
      type: mapTargetType(area?.shape ? area.shape : targetType),
      units: area?.unit ? mapUnit(area.unit) : undefined,
      value: (area ? area.value : count) || undefined,
      width: null,
    }

    const { damageParts, otherFormula, formulaSave } = sortRolls(
      attackDamage ?? [],
      damageRolls,
      otherRolls,
      savingThrows
    )

    const abilitySave = formulaSave ?? savingThrows.length > 0 ? savingThrows[0] : undefined

    const save = {
      scaling: 'flat',
      ability: abilitySave?.ability.slice(0, 3) || null,
      dc: abilitySave?.dc || null,
    }

    const damage = {
      parts: damageParts,
      versatile: versatile?.[0] ? versatile[0].formula : undefined,
    }

    const range = actionRange.normal
      ? {
          value: actionRange.normal,
          long: actionRange.max,
          units: actionRange.unit ? mapUnit(actionRange.unit) : undefined,
        }
      : { value: reach.value, units: reach.unit ? mapUnit(reach.unit) : undefined }

    const duration = {
      value: actionDuration.value || undefined,
      units: actionDuration.unit ? mapTimePeriod(actionDuration.unit) : undefined,
    }

    const actionType = save.dc ? 'save' : undefined

    const activationType = mapActivationType(actionActivation ?? '')
    const isLegendary = activationType === 'legendary'
    const activation = activationType
      ? {
          type: activationType,
          cost: isLegendary ? cost : 1,
        }
      : undefined

    const consume = isLegendary
      ? {
          type: 'attribute' as DND5e.ConsumeType,
          target: 'resources.legact.value',
          amount: cost,
        }
      : undefined

    const data: DeepPartial<data5e.Feat> = {
      description: {
        value: formatDescription(description),
      },
      activation,
      recharge: {
        charged: recharge.length > 0,
        value: recharge[0] as any, // Incorrect typing in type library, unable to override
      },
      consume,
      actionType,
      range,
      duration,
      damage,
      formula: otherFormula,
      ability: ability as DND5e.AbilityType,
      attackBonus,
      target,
      save,
      uses: {
        per: limitedUses.reset || undefined,
        value: limitedUses.count || undefined,
        max: limitedUses.count || undefined,
      },
    }

    return { ...base, ...data }
  }

export { createFeat }
