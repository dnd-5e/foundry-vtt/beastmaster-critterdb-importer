import { ItemDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/itemData'
import { Spellcasting } from '@nystik/critterdb-parser'
import { getFeatTemplate } from '@utils'
import { format } from 'path/posix'
import { formatDescription } from 'src/scripts/import/format/format-description'

const createSpellcastingData = (feature: Spellcasting): DeepPartial<data5e.Feat> => {
  const base = getFeatTemplate()

  const { description } = feature

  const data: DeepPartial<data5e.Feat> = {
    description: {
      value: formatDescription(description),
    },
  }

  return { ...base, ...data }
}

const createSpellcasting = (feature: Spellcasting, img?: string): DeepPartial<ItemDataConstructorData> => {
  const { name } = feature

  const type = 'feat'

  const data = createSpellcastingData(feature)

  return {
    name,
    type,
    img,
    data,
  }
}

export { createSpellcasting }
