import { ItemData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/module.mjs'
import { AbilityScoreModifiers, Spell, Spellcasting, Statblock5e } from '@nystik/critterdb-parser'
import { calculateAttackBonus, normalizeName, SpellMap } from '@utils'

const notesPattern = /\(.*\)/g
const removeNotes = (name: string) => name.replace(notesPattern, '').trimEnd()

const createSpell =
  ({ stats: { abilityScoreModifiers, proficiencyBonus } }: Statblock5e) =>
  (spellMap: SpellMap) =>
  ({ name, atWill, uses }: Spell, { innate, pact, ability, bonus, save }: Spellcasting): Partial<ItemData> => {
    const item: Partial<ItemData> = { ...spellMap[normalizeName(removeNotes(name))] }
    const data: data5e.Spell = item.data as data5e.Spell

    const mode: DND5e.Preparation = uses > 0 ? 'innate' : atWill ? 'atwill' : pact ? 'pact' : 'prepared'

    data.preparation = { mode, prepared: !(atWill && uses === 0) }
    if (uses > 0) {
      data.uses.max = uses
      data.uses.value = uses
      data.uses.per = 'day'
    }

    const spellmod: number = abilityScoreModifiers[ability.toLowerCase() as keyof AbilityScoreModifiers]
    const attackBonus = calculateAttackBonus(spellmod, proficiencyBonus)(bonus)

    data.ability = ability.toLowerCase().slice(0, 3)
    data.attackBonus = attackBonus

    data.save.scaling = 'flat'
    data.save.dc = save

    item.data = data

    const notes = name.match(notesPattern)
    item.name = `${item.name}${notes ? ' ' + notes : ''}`

    //@ts-ignore
    item.effects = []

    delete item._id
    delete item.permission
    return item
  }

const createSpells =
  (statblock: Statblock5e) =>
  (spellMap: SpellMap, features: Spellcasting[]): Partial<ItemData>[] => {
    const spellFactory = createSpell(statblock)(spellMap)

    let allSpells: Partial<ItemData>[] = []
    for (const f of features) {
      allSpells = [...allSpells, ...f.spells.map((s) => spellFactory(s, f))]
    }

    return allSpells
  }

export { createSpells }
