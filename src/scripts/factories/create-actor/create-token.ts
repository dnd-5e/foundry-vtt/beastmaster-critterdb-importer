import { TokenDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/tokenData'
import { Critter } from '@nystik/critterdb-parser'
import { normalizeName } from '@utils'
import { PACKAGE_KEY } from 'src/scripts/module-config'
import { TOKEN_DATA } from './constructor-data'

const tokenSize = (size: string): number => {
  return (
    {
      tiny: 0.5,
      small: 1,
      large: 2,
      huge: 3,
      gargantuan: 4,
    }[size.toLowerCase()] ?? 1
  )
}

const createToken = (critterData: Critter, img?: string): TokenDataConstructorData => {
  const base = { ...TOKEN_DATA }

  const {
    statblock: {
      name,
      details: { size },
      traits: { senses },
    },
  } = critterData

  const grid = tokenSize(size)

  const dimSight = senses.find((s) => normalizeName(s.name) === 'darkvision')?.value ?? 0
  const brightSight = Math.max(
    0,
    ...senses
      .filter(
        (s) =>
          normalizeName(s.name) === 'truesight'
          || normalizeName(s.name) === 'blindsight'
          || normalizeName(s.name) === 'tremorsense'
      )
      .map((s) => s.value)
  )

  const imported: Partial<TokenDataConstructorData> = {
    name,
    img,
    width: grid,
    height: grid,
    vision: true,
    dimSight,
    brightSight,
  }

  return { ...base, ...imported }
}

export { createToken }
