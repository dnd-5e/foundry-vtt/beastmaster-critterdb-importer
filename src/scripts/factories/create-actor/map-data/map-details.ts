import { mapCreatureType } from '@factories/common'
import { Critter, Details, Statblock5e } from '@nystik/critterdb-parser'

const mapDetails = (
  statblock: Statblock5e
): Partial<data5e.NpcDetails & data5e.CommonDetails & data5e.CreatureDetails> => {
  const {
    details: {
      race: { type, subtype, swarmsize },
      cr,
      xp,
      alignment,
      environment,
    },
    abilities: { spellcastingFeatures },
  } = statblock

  const value = mapCreatureType(type)

  const spellLevel = (spellcastingFeatures.find((f) => f.level)?.level ?? 0) as DND5e.SpellLevel

  return {
    type: {
      value,
      subtype: subtype.join(', '),
      swarm: swarmsize,
      custom: !value && type ? type : '',
    },
    environment,
    cr,
    xp: { value: xp },
    spellLevel,
    alignment: alignment as DND5e.Alignment, //possible typedef error in foundry-vtt-dnd5e-types
  }
}

export { mapDetails }
