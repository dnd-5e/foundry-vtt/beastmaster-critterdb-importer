import { mapSkillType } from '@factories/common'
import { AbilityScores, Stats } from '@nystik/critterdb-parser'
import { calculateSkillBonus } from '@utils'

//TODO: Will need patching for missing proficiency data
const mapSkills = ({ skills, abilityScoreModifiers, proficiencyBonus }: Stats): { [key: string]: data5e.Skill } => {
  const mapped: { [key: string]: data5e.Skill } = {}

  for (const s of skills) {
    const { name, ability, proficient, expertise } = s

    const key = mapSkillType(name)
    if (key) {
      const bonus = calculateSkillBonus(abilityScoreModifiers[ability as keyof AbilityScores], proficiencyBonus)(s)
      mapped[key] = {
        value: expertise ? 2 : proficient ? 1 : 0,
        bonuses: bonus ? { check: bonus } : undefined,
        ability: ability.slice(0, 3),
      }
    }
  }

  return mapped
}
export { mapSkills }
