import { Statblock5e } from '@nystik/critterdb-parser'
import { mapArmorClass } from './map-armor-class'
import { mapMovement } from './map-movement'
import { mapSenses } from './map-senses'

const mapAttributes = (statblock: Statblock5e): data5e.CommonAttributes & data5e.CreatureAttributes => {
  const {
    defense: { hitPoints, hitDieSize, hitDieCount, hitPointBonus },
    stats: { abilityScoreModifiers },
    traits: { speed, senses },
  } = statblock

  return {
    ac: mapArmorClass(statblock.defense),
    hp: {
      formula: `${hitDieCount}d${hitDieSize} + ${hitPointBonus}`,
      value: hitPoints,
      min: 0,
      max: hitPoints,
      temp: 0,
      tempmax: 0,
    },
    init: {
      value: abilityScoreModifiers.dexterity,
      bonus: 0,
    },
    movement: mapMovement(speed),
    senses: mapSenses(senses),
    spellcasting: '',
  }
}

export { mapAttributes }
