import { Defense } from '@nystik/critterdb-parser'

const mapArmorClass = (defense: Defense): data5e.ChaClass => {
  const { armorClass, armorType } = defense
  const calc = mapArmorClassCalc(armorType)
  return {
    value: 0,
    flat: calc === 'natural' || calc === 'flat' ? armorClass : null,
    calc,
    formula: '',
  }
}

const mapArmorClassCalc = (armorType: string): string => {
  switch (armorType) {
    case 'Natural Armor':
      return 'natural'
  }
  return 'flat'
}

export { mapArmorClass }
