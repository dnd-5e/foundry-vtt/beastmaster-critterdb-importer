import { mapUnit } from '@factories/common'
import { Sense } from '@nystik/critterdb-parser'

const mapSenses = (critterSenses: Sense[]): data5e.Senses => {
  const units = mapUnit(critterSenses.find((s) => s.unit)?.unit || '')
  const senses: any = {
    darkvision: 0,
    blindsight: 0,
    tremorsense: 0,
    truesight: 0,
    units,
    special: '',
  }
  for (const s of critterSenses) {
    let isType = false
    for (const type in senses) {
      if (s.name.toLowerCase().includes(type)) {
        isType = true
        senses[type] = s.value
      }
    }
    if (!isType) {
      senses.special += `${s}, `
    }
  }
  return senses
}

export { mapSenses }
