import { mapUnit } from '@factories/common'
import { Speed } from '@nystik/critterdb-parser'

const mapMovement = (movements: Speed[]): data5e.Movement => {
  const units = mapUnit(movements.find((s) => s.unit)?.unit ?? '')
  const movement: any = {
    burrow: 0,
    climb: 0,
    fly: 0,
    swim: 0,
    walk: 30,
    units,
    hover: false,
  }

  for (const m of movements) {
    let isType = false
    for (const type in movement) {
      if (m.name.toLowerCase().includes(type)) {
        isType = true
        movement[type] = m.value
      }
    }
    if (!isType) {
      movement.walk = m.value
    }
  }
  return movement
}

export { mapMovement }
