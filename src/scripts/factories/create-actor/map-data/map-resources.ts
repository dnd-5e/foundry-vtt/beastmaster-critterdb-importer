import { Statblock5e } from '@nystik/critterdb-parser'

const mapResources = (statblock: Statblock5e): data5e.NpcResources => {
  const {
    stats: { legendaryResistances },
    abilities: {
      legendaryActions: { charges },
    },
  } = statblock

  return {
    lair: { value: 0, initiative: 0 },
    legact: { value: charges, max: charges },
    legres: { value: legendaryResistances, max: legendaryResistances },
  }
}

export { mapResources }
