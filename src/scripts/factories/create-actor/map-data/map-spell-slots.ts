import { Spellcasting } from '@nystik/critterdb-parser'

//TODO: rename to sumSpellSlots maybe?
const mapSpellSlots = (features: Spellcasting[]): Record<DND5e.SpellType, data5e.SpellValue> => {
  const slots: Record<string, data5e.SpellValue> = {}

  for (const f of features) {
    const { pact } = f
    for (const [level, num] of Object.entries(f.slots)) {
      const key = pact ? 'pact' : `spell${level}`

      const value = slots[key] ? slots[key].value + num : num
      slots[key] = {
        value,
        override: null,
      }
    }
  }

  return slots
}

export { mapSpellSlots }
