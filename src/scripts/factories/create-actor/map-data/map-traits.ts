import { getConditionsKeys, getDamageTypesKeys, mapSize } from '@factories/common'
import { getLanguagesKeys } from '@factories/common/get-languages'
import { Statblock5e } from '@nystik/critterdb-parser'

const mapTraits = (statblock: Statblock5e): data5e.CommonTraits & data5e.CreatureTraits => {
  const {
    details: { size },
    traits: { languages },
    defense: { conditionImmunities, damageResistances, damageImmunities, damageVulnerabilities },
  } = statblock

  return {
    size: mapSize(size.toLowerCase()),
    di: mapTrait(damageImmunities, getDamageTypesKeys()),
    dr: mapTrait(damageResistances, getDamageTypesKeys()),
    dv: mapTrait(damageVulnerabilities, getDamageTypesKeys()),
    ci: mapTrait(conditionImmunities, getConditionsKeys()),
    languages: mapTrait(languages, getLanguagesKeys()),
  }
}

const mapTrait = <T extends string>(data: string[], keys: T[]): data5e.ArmorProf => {
  const value: T[] = []
  const customValues: string[] = []

  for (const d of data) {
    if (keys.includes(d.toLowerCase() as T)) {
      value.push(d.toLowerCase() as T)
    } else if (d !== '') {
      customValues.push(d)
    }
  }

  return {
    value,
    custom: customValues.join(';'),
  }
}

export { mapTraits }
