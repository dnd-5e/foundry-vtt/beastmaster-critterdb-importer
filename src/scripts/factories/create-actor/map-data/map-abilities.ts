import { SavingThrow, Stats } from '@nystik/critterdb-parser'
import { calculateSaveBonus } from '@utils'
import { savingThrowLookup } from '@utils'

const isProficientSave = (saves: SavingThrow[], ability: string): 0 | 1 => {
  return saves.find((s) => s.ability === ability)?.proficient ? 1 : 0
}

const mapAbilities = (stats: Stats): data5e.CommonAbilities => {
  const { abilityScores, abilityScoreModifiers, savingThrows, proficiencyBonus } = stats

  const saves = savingThrowLookup(savingThrows)
  return {
    str: {
      value: abilityScores.strength,
      proficient: isProficientSave(savingThrows, 'strength'),
      bonuses: {
        save: calculateSaveBonus(abilityScoreModifiers.strength, proficiencyBonus)(saves('strength')) || undefined,
      },
    },
    dex: {
      value: abilityScores.dexterity,
      proficient: isProficientSave(savingThrows, 'dexterity'),
      bonuses: {
        save: calculateSaveBonus(abilityScoreModifiers.dexterity, proficiencyBonus)(saves('dexterity')) || undefined,
      },
    },
    con: {
      value: abilityScores.constitution,
      proficient: isProficientSave(savingThrows, 'constitution'),
      bonuses: {
        save:
          calculateSaveBonus(abilityScoreModifiers.constitution, proficiencyBonus)(saves('constitution')) || undefined,
      },
    },
    int: {
      value: abilityScores.intelligence,
      proficient: isProficientSave(savingThrows, 'intelligence'),
      bonuses: {
        save:
          calculateSaveBonus(abilityScoreModifiers.intelligence, proficiencyBonus)(saves('intelligence')) || undefined,
      },
    },
    wis: {
      value: abilityScores.wisdom,
      proficient: isProficientSave(savingThrows, 'wisdom'),
      bonuses: {
        save: calculateSaveBonus(abilityScoreModifiers.wisdom, proficiencyBonus)(saves('wisdom')) || undefined,
      },
    },
    cha: {
      value: abilityScores.charisma,
      proficient: isProficientSave(savingThrows, 'charisma'),
      bonuses: {
        save: calculateSaveBonus(abilityScoreModifiers.charisma, proficiencyBonus)(saves('charisma')) || undefined,
      },
    },
  }
}

export { mapAbilities }
