import { ActorDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/actorData'
import { PrototypeTokenDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/prototypeTokenData'

const TOKEN_DATA: PrototypeTokenDataConstructorData = {
  flags: {},
  name: '',
  displayName: 20,
  img: 'icons/svg/mystery-man.svg',
  width: 1,
  height: 1,
  scale: 1,
  vision: false,
  dimSight: 0,
  brightSight: 0,
  dimLight: 0,
  brightLight: 0,
  sightAngle: 360,
  lightAngle: 360,
  disposition: -1,
  displayBars: 40,
  bar1: {
    attribute: 'attributes.hp',
  },
  randomImg: false,
}

const NPC_ACTOR_DATA: ActorDataConstructorData = {
  name: '',
  type: 'npc',
  data: {},
  flags: {
    dnd5e: {},
  },
  img: 'icons/svg/mystery-man.svg',
  items: [],
  effects: [],
  token: TOKEN_DATA,
}

export { NPC_ACTOR_DATA, TOKEN_DATA }
