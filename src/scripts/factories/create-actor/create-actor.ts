import { ActorDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/actorData'
import { Critter } from '@nystik/critterdb-parser'
import { getNpcTemplate } from '@utils'
import { PACKAGE_KEY } from 'src/scripts/module-config'
import { NPC_ACTOR_DATA } from './constructor-data'
import { mapAbilities, mapAttributes, mapDetails, mapTraits, mapSkills, mapResources } from './map-data'
import { mapSpellSlots } from './map-data'

interface CritterMeta {
  bestiaryId?: string
  publishedBestiaryId?: string
  critterId: string
  hash: string
}

const createActor = (critterData: Critter, img?: string): ActorDataConstructorData => {
  const base = { ...NPC_ACTOR_DATA, data: getNpcTemplate() }

  const {
    _id,
    bestiaryId,
    publishedBestiaryId,
    statblock,
    statblock: {
      name,
      stats,
      stats: { skills },
      abilities: { spellcastingFeatures },
    },
  } = critterData

  const meta: CritterMeta = {
    bestiaryId,
    publishedBestiaryId,
    critterId: _id,
    hash: '',
  }

  const { innate, pact, level, save, bonus, ability } =
    spellcastingFeatures.sort((a, b) => b.level - a.level)?.[0] ?? {}

  const data: DeepPartial<data5e.Npc> = {
    details: { ...mapDetails(statblock), spellLevel: (level as DND5e.SpellLevel) ?? 0 },
    attributes: { ...mapAttributes(statblock), spellcasting: ability?.slice(0, 3) ?? 'int' },
    abilities: mapAbilities(stats),
    traits: mapTraits(statblock),
    skills: mapSkills(stats),
    spells: mapSpellSlots(spellcastingFeatures),
    resources: mapResources(statblock),
  }

  const imported: Partial<ActorDataConstructorData> = {
    name,
    flags: {
      [PACKAGE_KEY]: meta,
    },
    data,
    items: [],
    effects: [],
    img,
  }

  return { ...base, ...imported }
}

export { createActor }
